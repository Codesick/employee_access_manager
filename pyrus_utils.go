package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/Jeffail/gabs"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"time"
)

type PyrusComment struct {
	ApprovalsChoice string `json:"approval_choice"`
	Text            string `json:"text"`
}

type auth struct {
	AccessToken string `json:"access_token"`
}

func FormRouter(formId string, jsonParsed *gabs.Container) error {
	switch formId {
	case "151515":
		err := TaskHandlerForForm151515(jsonParsed)
		return err
	case "912300":
		err := TaskHandlerForForm912300(jsonParsed)
		return err
	case "000000":
		return errors.New("this is template")
	default:
		return errors.New(fmt.Sprintf("no route's for form %v", formId))
	}
}

func TaskHandlerForForm912300(jsonParsed *gabs.Container) error {
	taskIdFloat, success := jsonParsed.Path("task.id").Data().(float64)
	if !success {
		log.Warningf("Couldn't parse taskId: %v", success)
		err := errors.New("couldn't parse taskId")
		return err
	}
	taskId := strconv.Itoa(int(taskIdFloat))
	success = saveNewHiredEmployeeList(taskId)
	if !success {
		log.Warning("Couldn't store file from  task")
		err := errors.New("couldn't store file from task")
		return err
	}
	//initPyrusAuth()
	err := PyrusApproveTask(taskId, "stored")
	if err != nil {
		log.Warningf("Couldn't approve task: %v", taskId)
		err := errors.New("couldn't approve task")
		return err
	}
	return nil
}

func TaskHandlerForForm151515(jsonParsed *gabs.Container) error {
	userData, err := PyrusDataToStruct(jsonParsed)
	if err != nil {
		log.Warning("Couldn't parse task: ", err)
		return err
	}

	log.Info(userData)
	pyrusRecord := Pyrus{
		Id:		userData["PyrusId"].(string),
		FormId:	userData["FormId"].(string),
		Status: 1,
		Message: "",
	}
	userRecord := AdUser{
		Snils:    userData["Snils"].(string),
		PyrusTask: pyrusRecord,
		Fio:      userData["Fio"].(string),
		Ad:       userData["Ad"].(string),
		Email:    userData["Email"].(string),
		Status:   1,
	}
	AddUser(userRecord)
	return nil
}

func PyrusDataToStruct(jsonParsed *gabs.Container) (map[string]interface{}, error) {
	var taskId float64
	var success bool
	userMap := make(map[string]interface{})
	taskId, success = jsonParsed.Path("task.id").Data().(float64)
	if !success {
		log.Warning("Couldn't parse taskId: ", success)
		err := errors.New("couldn't parse taskId")
		return make(map[string]interface{}), err
	}
	userMap["PyrusId"] = strconv.Itoa(int(taskId))

	var formId float64
	formId, success = jsonParsed.Path("task.form_id").Data().(float64)
	if !success {
		log.Warning("formId: ", success)
		err := errors.New("couldn't parse formId")
		return make(map[string]interface{}), err
	}

	userMap["FormId"] = strconv.Itoa(int(formId))
	userMap["PyrusId"] = strconv.Itoa(int(taskId))
	log.Info(fmt.Sprintf("taskId: %v", userMap["PyrusId"]))
	log.Info(fmt.Sprintf("formId: %v", userMap["FormId"]))

	fields, err := jsonParsed.S("task", "fields").Children()
	if err != nil {
		log.Warning("Couldn't parse task fields: ", err)
		return make(map[string]interface{}), err
	}

	var valueId float64
	var valId int
	var valueData string
	for _, object := range fields {
		valueId, _ = object.Path("id").Data().(float64)
		valId = int(valueId)
		if IsElementInIterator(valId, FIELD_MAP) {
			valueData, success = object.Path("value").Data().(string)
			if !success {
				log.Warning(fmt.Sprintf("Couldn't parse %v", FIELD_MAP[valId]))
			}
			log.Info(fmt.Sprintf("%v: %v", FIELD_MAP[valId], valueData))
			userMap[FIELD_MAP[valId]] = valueData
		}
	}
	return userMap, nil
}

func PurusGetToken()(token string, err error){
	tmpUrl := PYRUS_URL_V4 + "auth"
	req, _ := http.NewRequest("GET", tmpUrl, nil)

	values := req.URL.Query()
	values.Set("login", PYRUS_USER)
	values.Set("security_key", PYRUS_KEY)
	req.URL.RawQuery = values.Encode()
	client := &http.Client{
		Timeout: time.Second * 10,
	}
	resp, err := client.Do(req)
	if err != nil {
		log.Error(err)
		return
	} else {
		log.Debug(resp)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
		return
	}

	pAuth := auth{}
	err = json.Unmarshal(body, &pAuth)
	//jsonParsed, err := gabs.ParseJSON(body)
	if err != nil {
		log.Fatal(err)
		return
	} else {
		log.Debugf("pAuth: %v", pAuth)
		token = "Bearer " + pAuth.AccessToken
	}
	return token, nil
}

func PyrusApproveTask(pyrusId string, msg string) (err error) {
	pyrusMsg := PyrusComment{
		ApprovalsChoice: "approved",
		Text:            msg,
	}
	taskURL := PYRUS_URL_V4 + "tasks/" + pyrusId + "/comments"

	log.Info(pyrusMsg)
	marshaledPyrusMsg, err := json.Marshal(pyrusMsg)
	if err != nil {
		log.Error(err)
	} else {
		bufferedPyrusMsg := bytes.NewBuffer(marshaledPyrusMsg)
		req, _ := http.NewRequest("POST", taskURL, bufferedPyrusMsg)
		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("Authorization", PYRUS_ACCESS_TOKEN)

		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			log.Error(err)
			return err
		} else {
			log.Info(resp)
		}

		log.Infof("Pyrus task-%v comment status code: %v", pyrusId, resp.StatusCode)

		if !(resp.StatusCode >= 200 && resp.StatusCode < 300) {
			return errors.New("couldn't delivery message to pyrus")
		}
		//defer resp.Body.Close()
		//body, _ := ioutil.ReadAll(resp.Body)
		//
		//jsonParsed, err := gabs.ParseJSON(body)
		//if err != nil {
		//	log.Fatal(err)
		//} else {
		//	log.Info(jsonParsed)
		//}

	}
	return nil
}

func GetPyrusTask(pyrusId string)  (jsonParsed *gabs.Container, err error) {
	taskURL := PYRUS_URL_V4 + "tasks/" + pyrusId
	req, _ := http.NewRequest("GET", taskURL, nil)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", PYRUS_ACCESS_TOKEN)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Error(err)
		return jsonParsed, err
	} else {
		log.Info(resp)
	}

	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	jsonParsed, err = gabs.ParseJSON(body)
	if err != nil {
		log.Fatal(err)
	} else {
		log.Info(jsonParsed)
	}


	return jsonParsed, err
}

func DownloadFileFromPyrus(fileId string, pathToStore *string) error {
	fileUrl := PYRUS_URL_V4 + "files/download/" + fileId
	if pathToStore == nil {
		*pathToStore = HIRED_LIST_PATH
	}
		//Create the file
	out, err := os.Create(*pathToStore)
	if err != nil {
		return err
	}
	defer out.Close()

	req, _ := http.NewRequest("GET", fileUrl, nil)
	req.Header.Set("Authorization", PYRUS_ACCESS_TOKEN)
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Error(err)
		return err
	} else {
		log.Infof("reciving file from pyrus status code %v", resp.StatusCode)
	}
	defer resp.Body.Close()

	//Write the body to file
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return err
	}
	return nil
}

func getAttacchmentIdArray(jsonParsed *gabs.Container) (attachmentIds []string) {
	jsonArray := jsonParsed.Path("task.attachments.id")
	log.Debug(jsonArray)
	children, _ := jsonArray.Children()
	var attachId string
	for _, child := range children {
		attachId = strconv.Itoa(int(child.Data().(float64)))
		attachmentIds = append(attachmentIds, attachId)
	}
	return attachmentIds
}

func saveNewHiredEmployeeList(taskId string) (success bool) {
	success = false
	taskJson, err := GetPyrusTask(taskId)
	if err != nil {
		return
	}
	attachmentIds := getAttacchmentIdArray(taskJson)
	fileCount := len(attachmentIds)
	if fileCount > 0 {
		err := DownloadFileFromPyrus(attachmentIds[fileCount - 1], &HIRED_LIST_PATH)
		if err == nil {
			success = true
		}
	}
	return
}