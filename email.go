package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"time"
)

type Alert struct {
	//time = fields.DateTime(required=True)
	Time    string `json:"time"`
	Queue   string `json:"queue"`
	Service string `json:"service"`
	Host    string `json:"host"`
	Type    string `json:"type"`
	Msg     string `json:"msg"`
	Data    string `json:"data"`
}

func SendReportToMailManager(msg string) {
	alert := Alert{
		Time:    time.Now().Format(time.RFC3339Nano), //"2006-01-02T15:04:05.999999999+07:00"),
		Queue:   "employee_manager",
		Service: "employee_manager",
		Host:    "hq-pyrus",
		Type:    "fail",
		Msg:     msg,
	}
	log.Info(alert)
	marshaledAlert, err := json.Marshal(alert)
	if err != nil {
		log.Error(err)
	} else {
		bufferedMsg := bytes.NewBuffer(marshaledAlert)
		resp, err := http.Post(ALERT_URL, "application/json; charset=utf-8", bufferedMsg)
		if err != nil {
			log.Error(err)
		} else {
			log.Info(resp)
		}
	}
}

