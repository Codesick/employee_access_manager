package main

import (
	"errors"
	"fmt"
	"github.com/Jeffail/gabs"
	"github.com/gorilla/mux"
	"github.com/op/go-logging"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/satori/go.uuid"
	"golang.org/x/net/context"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
)

//type key int
const requestIDKey int = 0

var log = logging.MustGetLogger("basic")
var backendList []logging.Backend
var format = logging.MustStringFormatter(
	"%{color}%{time:15:04:05.000} %{shortfunc} ▶ " +
		"%{level:.6s} %{shortfile}%{color:reset} %{message}",
)
var consoleBackend = logging.NewLogBackend(os.Stdout, "", 0)
var consoleBackendFormatter = logging.NewBackendFormatter(consoleBackend, format)
var consoleBackendLeveled = logging.AddModuleLevel(consoleBackendFormatter)

func loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		// Do stuff here
		//      log.Info(req.Body)
		log.Info(req.Body)
		// Call the next handler, which can be another middleware in the chain, or the final handler.
		next.ServeHTTP(w, req)
	})
}

func newContextWithRequestID(ctx context.Context, req *http.Request) context.Context {
	// reqUuid := uuid.Must(uuid.NewV4())
	reqUuid, _ := uuid.NewV4()
	reqID := reqUuid.String()
	return context.WithValue(ctx, requestIDKey, reqID)
}

func contextMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		ctx := newContextWithRequestID(req.Context(), req)
		next.ServeHTTP(rw, req.WithContext(ctx))
	})
}

func pyrusHandler(w http.ResponseWriter, req *http.Request) {
	reqID := RequestIDFromContext(req.Context())
	log.Info(fmt.Sprintf("request_id: %v", reqID))
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		log.Error(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	hexBody := BodyToHex(body)
	signature := strings.ToLower(req.Header.Get("X-Pyrus-Sig"))
	clientIP := req.Header.Get("X-Forwarded-For")
	if hexBody != signature {
		msg := fmt.Sprintf("Wrong signature:"+
			"\r\nsig=%v\r\nhexbody=%v\r\nIP: %v", signature, hexBody, clientIP)
		log.Warning(msg)
		err := errors.New(msg)
		go SendReportToMailManager(msg)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	jsonParsed, err := gabs.ParseJSON(body)
	if err != nil {
		log.Warningf("Couldn't parse json: %v", err)
		go SendReportToMailManager("Couldn't parse json")
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	formIdFloat, success := jsonParsed.Path("task.form_id").Data().(float64)
	if !success {
		log.Warningf("formIdFloat extracted state: %v", success)
		err := errors.New("couldn't parse formIdFloat from task")
		go SendReportToMailManager("Couldn't parse formIdFloat")
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	formId := strconv.Itoa(int(formIdFloat))

	switch {
	case IsElementInIterator(formId, FORM_FILTER):
		err := FormRouter(formId, jsonParsed)
		if err != nil {
			go SendReportToMailManager(err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	default:
		log.Warningf("catched task from form %v, "+
			"not in list of listening forms: %v", formId, FORM_FILTER)
		err := errors.New("catched task not in list of listening forms")
		go SendReportToMailManager("Catched task not in list of listening forms")
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return

	}

	w.Header().Set("Server", "I'm watching you")
	w.WriteHeader(200)

}

func CreateTestUser() {
	newPyr := Pyrus{
		Id:      "000007",
		FormId:  "102109",
		Status:  101,
		Message: "Test",
	}
	newUser := AdUser{
		Snils:     "003-400-789 0",
		PyrusTask: newPyr,
		PyrusId:   "000007",
		Fio:       "Mista Mer",
		Ad:        "mercy",
		Email:     "mercy@sovcombank.ru",
		Status:    1,
	}
	log.Info(newUser)
	AddUser(newUser)
}

func main() {
	backendList = []logging.Backend{}
	consoleBackendLeveled.SetLevel(logging.DEBUG, "")
	backendList = append(backendList, consoleBackendLeveled)
	if FILE_LOG_ON {
		logFile, _ := os.OpenFile("./application.log", os.O_APPEND|os.O_CREATE|os.O_RDWR, 0644)
		fileBackend := logging.NewLogBackend(logFile, "", 0)
		fileBackendFormatter := logging.NewBackendFormatter(fileBackend, format)
		fileBackendLeveled := logging.AddModuleLevel(fileBackendFormatter)
		fileBackendLeveled.SetLevel(logging.DEBUG, "")
		backendList = append(backendList, fileBackendLeveled)
	}

	// Set the backends to be used.
	logging.SetBackend(backendList...)

	InitialMigration()
	defer db.Close()
	go ScheduleTask()
	log.Info("running adbot serivce")

	router := mux.NewRouter()

	router.Handle("/metrics", promhttp.Handler())
	router.HandleFunc("/ad_task", pyrusHandler).Methods("POST")
	router.Use(contextMiddleware)
	router.Use(loggingMiddleware)

	//CreateTestUser()
	//log.Infof("task is %v", GetTask("27774491"))
	//PyrusApproveTask("27774491", "1st try")
	//go SendReportToMailManager("start adbot manager")
	//CheckUsers()
	//GetPyrusTask("27774491")
	CheckTasks()
	bildUrl := fmt.Sprintf("%v:%v", BILD_HOST, BILD_PORT)
	err := http.ListenAndServe(bildUrl, router)
	log.Error(err.Error)
}
