package main

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

var db *gorm.DB

type AdUser struct {
	gorm.Model
	Snils     string `gorm:"index:snils"`
	PyrusTask Pyrus  `gorm:"foreignkey:PyrusId;association_foreignkey:Id"`
	PyrusId   string `gorm:"unique;not null"`
	Fio       string
	Ad        string
	Email     string
	Status    uint `gorm:"index:status"`
}

type Pyrus struct {
	Id      string `gorm:"primary_key"`
	FormId  string
	Status  uint
	Message string
}

func AddUser(user AdUser) {
	// need check for existing by pyrus_id
	if db.Debug().Where("pyrus_id = ?", user.PyrusId).First(&AdUser{}).RecordNotFound() {
		log.Info(fmt.Sprintf("Create new user with ID %v", user.ID))
		db.Debug().Create(&user)
	} else {
		log.Info(fmt.Sprintf("Update user with ID %v", user.ID))
		db.Debug().Model(&AdUser{}).Where("pyrus_id = ?", user.PyrusTask.Id).Updates(user)
	}
}

func ChangeUserStatus(user AdUser, status uint) {
	db.Model(&user).Where("pyrus_id = ?", user.PyrusId).Update("status", status)
}

func ChangePyrusTaskStatus(pyrusId string, status uint, msg string) {
	db.Model(&Pyrus{}).Where("id = ?", pyrusId).
		Update(Pyrus{Status: status, Message: msg})
}

func GetTask(pyrusID string) (task Pyrus) {
	db.First(&task, pyrusID)
	return
}

func InitialMigration() {
	// postgres dialect
	//connectionString := fmt.Sprintf("host=%v port=%v user=%v dbname=%v password=%v",
	//	DB_HOST, DB_PORT, DB_LOGIN, DB_NAME, DB_PASSWORD,
	//)

	// mysql dialect
	connectionString := fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?"+
		"charset=utf8mb4&parseTime=True&loc=Local",
		DB_LOGIN, DB_PASSWORD, DB_HOST, DB_PORT, DB_NAME,
	)
	log.Info(connectionString)
	var err error
	db, err = gorm.Open("mysql", connectionString)
	if err != nil {
		log.Error(err)
		panic("Failed to connect")
	}
	db.Set("gorm:table_options", "charset=utf8mb4")
	//db.DropTableIfExists(&AdUser{})
	//db.DropTableIfExists(&Pyrus{})
	if exist := db.HasTable(&AdUser{}); !exist {
		log.Info(fmt.Sprintf("AdUser db exsist - %v", exist))
		db.CreateTable(&AdUser{})
	}

	if exist := db.HasTable(&Pyrus{}); !exist {
		log.Info(fmt.Sprintf("AdUser db exsist - %v", exist))
		db.CreateTable(&Pyrus{})
	}

	db.AutoMigrate(&AdUser{})
	db.AutoMigrate(&Pyrus{})

}
