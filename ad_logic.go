package main

import (
	"fmt"
	"github.com/paleg/libadclient"
	"strings"
)

func BlockUser(user AdUser) (blocked bool, err error) {
	blocked = false
	err = nil
	log.Info(user)
	adclient.New()
	defer adclient.Delete()

	log.Info("ad logining")
	errLogin := adclient.Login(GetADAuthParams())
	if errLogin != nil {
		log.Warningf("Failed to AD login: %v\n", errLogin)
		return blocked, errLogin
	}

	log.Info("ad get user groups")
	userGroups, errGetGroups := adclient.GetUserGroups(user.Ad, false)
	if errGetGroups != nil {
		log.Warning(fmt.Sprintf("Failed to get object: %v\n", errGetGroups))
		return blocked, errGetGroups
	}

	log.Info(userGroups)
	for _, group := range userGroups {
		errRemove := adclient.GroupRemoveUser(group, user.Ad)
		if errRemove != nil {
			log.Warningf("Caught error while removing " +
				"user %v from group %v\n%v", user.Ad, group, errRemove)
		} else {
			blocked = true
			log.Infof("Successfuly removed user %v from group %v", user.Ad, group)
		}
	}

	errDisable := adclient.DisableUser(user.Ad)
	if errDisable != nil {
		message := fmt.Sprintf("Couldn't disable user %v\n%v", user.Ad, errDisable)
		log.Warning(message)
		go SendReportToMailManager(message)
	}

	userDN, errDnRead := adclient.GetObjectDN(user.Ad)
	if errDnRead != nil {
		log.Warningf("Couldn't read DN of user %v " +
			"and move user to retired container group\n%v", user.Ad, errDnRead)
		return blocked, errDnRead
	}

	retiredContainer := MakeRetiredContainer(userDN)
	errMoveToRetired := adclient.MoveUser(user.Ad, retiredContainer)
	if errMoveToRetired != nil {
		message := fmt.Sprintf("Couldn't move user %v to retired" +
			" group\n%v", user.Ad, errMoveToRetired)
		log.Warning(message)
		go SendReportToMailManager(message)
		return blocked, errMoveToRetired
	}
	return blocked, err
}

func MakeRetiredContainer(actualDN string) string {
	log.Debugf("%v", actualDN)
	dnBlocks := strings.Split(actualDN, ",")
	retriedDNBlocks := []string{"ou=retired", dnBlocks[cap(dnBlocks)-4],
	"ou=prankrc", "dc=prankrc", "dc=local"}
	retriedDN := strings.Join(retriedDNBlocks, ",")
	return retriedDN
}

func GetADAuthParams() (params adclient.ADConnParams) {
	params = adclient.DefaultADConnParams()
	params.Search_base = "dc=prankrc,dc=local"
	params.Binddn = AD_USER
	params.Bindpw = AD_PASSWORD
	params.Timelimit = 30
	params.Nettimeout = 30
	params.Secured = false
	params.Uries = append(params.Uries, adclient.LdapPrefix()+"hq-mp3")
	return
}

