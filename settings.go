package main

import (
	"fmt"
	"gopkg.in/ini.v1"
	"os"
	"strings"
)

var PYRUS_KEY string
var PYRUS_USER string
var PYRUS_URL_V4 = "https://api.pyrus.com/v4/"
var PYRUS_ACCESS_TOKEN string
var BILD_HOST = "0.0.0.0"
var BILD_PORT = "10000"
var DB_PORT = 5432
var DB_NAME = "employee"
var DB_LOGIN = "employee"
var DB_HOST string
var DB_PASSWORD string
var AD_USER string
var AD_PASSWORD string
var ALERT_URL = "http://" + "hq-onthego-app.soho.pop" + ":13313" + "/alert"
var HIRED_LIST_PATH = "./data.xls"
var FILE_LOG_ON = true
var USER_OBSERVING_DURATION float64 // hours
var FORM_FILTER []string

var CONCURENT = 20

var FIELD_MAP map[int]string
var USER_STATUS_MAP map[string]uint
var PYRUS_STATUS_MAP map[string]uint

// mysql -uemployee -p employee

func init() {
	initFromConfig()
	//or
	//initFromEnv()

	initPyrusAuth()
	makeFieldMap()
	makeStatusMap()
}

func initFromConfig() {
	cfg, err := ini.Load("./employee.ini")
	if err != nil {
		fmt.Printf("Fail to read file: %v", err)
		return
	}

	PYRUS_KEY = cfg.Section("pyrus").Key("key").String()
	PYRUS_USER = cfg.Section("pyrus").Key("user").String()
	DB_PORT, _ = cfg.Section("database").Key("port").Int()
	DB_NAME = cfg.Section("database").Key("name").String()
	DB_LOGIN = cfg.Section("database").Key("user").String()
	DB_HOST = cfg.Section("database").Key("host").String()
	DB_PASSWORD = cfg.Section("database").Key("password").String()
	AD_USER = cfg.Section("active_directory").Key("user").String()
	AD_PASSWORD = cfg.Section("active_directory").Key("password").String()
	HIRED_LIST_PATH = cfg.Section("shared_directory").Key("haired_list_path").String()

	USER_OBSERVING_DURATION, err = cfg.Section("pyrus").Key("observing_duration").Float64()
	if err != nil {
		USER_OBSERVING_DURATION = 1
	}
	tmpFormFilter := cfg.Section("pyrus").Key("form_filter").String()
	tmpFormFilter = strings.Replace(tmpFormFilter, " ", "", -1)
	FORM_FILTER = strings.Split(tmpFormFilter, ",")
}

func initFromEnv() {
	var exist = false
	DB_PASSWORD, exist = os.LookupEnv("EMPLOYEE_DB_PSSWD")
	if exist == false {
		DB_PASSWORD = "pass"
	}

	exist = false
	DB_HOST, exist = os.LookupEnv("EMPLOYEE_DB_HOST")
	if exist == false {
		DB_HOST = "localhost"
	}

	exist = false
	DB_LOGIN, exist = os.LookupEnv("EMPLOYEE_DB_USER")
	if exist == false {
		DB_LOGIN = "employee"
	}

	exist = false
	BILD_PORT, exist = os.LookupEnv("EMPLOYEE_PORT")
	if exist == false {
		BILD_PORT = "5432"
	}
}

func initPyrusAuth() {
	ptoken, err := PurusGetToken()
	if err != nil {
		log.Warningf("Couldn't get access token from Pyrus api %v", err)
	} else {
		PYRUS_ACCESS_TOKEN = ptoken
	}
}

func makeFieldMap() {
	FIELD_MAP = make(map[int]string)
	FIELD_MAP[1] = "Fio"
	FIELD_MAP[135] = "Snils"
	FIELD_MAP[9] = "Ad"
	FIELD_MAP[60] = "Email"
}

func makeStatusMap() {
	USER_STATUS_MAP = make(map[string]uint)
	USER_STATUS_MAP["OBSERVED"] = 1
	USER_STATUS_MAP["DISABLED"] = 6
	USER_STATUS_MAP["HAIRED"] = 9

	PYRUS_STATUS_MAP = make(map[string]uint)
	PYRUS_STATUS_MAP["OBSERVED"] = 1
	PYRUS_STATUS_MAP["WAITING_PYRUS_CONNECTION"] = 15
	PYRUS_STATUS_MAP["FINISHED"] = 200
}
