package main

import (
	"fmt"
	"github.com/jasonlvhit/gocron"
	"time"
)

var SEMAPHORE_CHAN = make(chan struct{}, CONCURENT)
var hairedPersonSnilsList []string

//fake blocker for NT system
//func BlockUser(user AdUser) (blocked bool, err error) {
//	blocked = true
//	err = nil
//	return
//}

func UserWorker(user AdUser) {
	log.Info(fmt.Sprintf("hello %v\n", user))
	timeDelta := time.Now().Sub(user.CreatedAt)
	log.Info(fmt.Sprintf("Hours delta: %v", timeDelta))
	if timeDelta.Hours() > USER_OBSERVING_DURATION {
		blocked, err := BlockUser(user)
		if err != nil {
			log.Warningf("Couldn't block user: %v\n%v", user.Ad, err)
			return
		}
		if blocked {
			log.Info("blocked")
			ChangeUserStatus(user, USER_STATUS_MAP["DISABLED"])
			message := fmt.Sprintf("%v blocked", user.Fio)
			var taskState = PYRUS_STATUS_MAP["WAITING_PYRUS_CONNECTION"]
			if err := PyrusApproveTask(user.PyrusId, message); err == nil {
				taskState = PYRUS_STATUS_MAP["FINISHED"]
			}
			ChangePyrusTaskStatus(user.PyrusId, taskState, message)
		} else {
			message := fmt.Sprintf("User %v already block", user.Ad)
			log.Info(message)
			go SendReportToMailManager(message)
		}

	} else {
		if IsElementInIterator(user.Snils, hairedPersonSnilsList) {
			log.Info(fmt.Sprintf("found user with snils %v", user.Snils))
			ChangeUserStatus(user, USER_STATUS_MAP["HAIRED"])
			message := fmt.Sprintf("%v haired", user.Fio)
			var taskState = PYRUS_STATUS_MAP["WAITING_PYRUS_CONNECTION"]
			if err := PyrusApproveTask(user.PyrusId, message); err == nil {
				taskState = PYRUS_STATUS_MAP["FINISHED"]
			}
			ChangePyrusTaskStatus(user.PyrusId, taskState, message)
		} else {
			log.Info(fmt.Sprintf("No coincidence with user %v", user.Fio))

		}
	}

}

func TaskWorker(task Pyrus) {
	log.Info("task in process")
	if err := PyrusApproveTask(task.Id, task.Message); err == nil {
		log.Infof("Pyrus task-%v approved", task.Id)
		ChangePyrusTaskStatus(
			task.Id,
			PYRUS_STATUS_MAP["FINISHED"],
			task.Message)
	} else {
		log.Error(err)
	}
}

func DoWorkThroughSemaphore(task Pyrus) {
	SEMAPHORE_CHAN <- struct{}{}
	go func() {
		defer func() {
			TaskWorker(task)
			<-SEMAPHORE_CHAN // read to release a slot
		}()
	}()
}

func CheckUsers() {
	//initPyrusAuth()
	fmt.Println("I am runnning CheckUsers.")
	var users []AdUser
	db.Where("status = ?", USER_STATUS_MAP["OBSERVED"]).Find(&users)
	log.Info(users)
	if cap(users) > 0 {
		hairedPersonSnilsList, _ = ReadSnilsFromXlsFile(HIRED_LIST_PATH)
		//log.Info(hairedPersonSnilsList)
		for _, user := range users {
			log.Info(user.PyrusId)
			UserWorker(user)
		}
	}
}

func CheckTasks() {
	//initPyrusAuth()
	fmt.Println("I am runnning CheckTasks.")
	var tasks []Pyrus
	db.Where("status = ?", PYRUS_STATUS_MAP["WAITING_PYRUS_CONNECTION"]).Find(&tasks)
	log.Info(tasks)
	if cap(tasks) > 0 {
		for _, task := range tasks {
			log.Info(fmt.Sprintf("Task ID: %v, message: %v", task.Id, task.Message))
			DoWorkThroughSemaphore(task)
		}
	}
}

func ScheduleTask() {
	gocron.Every(1).Day().At("9:00").Do(CheckUsers)
	gocron.Every(1).Day().At("6:00").Do(initPyrusAuth)
	gocron.Every(1).Day().At("18:00").Do(CheckUsers)
	gocron.Every(15).Minutes().Do(CheckTasks)
	//gocron.Every(30).Minutes().Do(CheckUsers)
	//gocron.Every(1).Hours().Do(CheckTasks)
	<-gocron.Start()
}
