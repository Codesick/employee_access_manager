package main

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"github.com/extrame/xls"
	"golang.org/x/net/context"
	"math"
	"reflect"
)

func IsElementInIterator(keyI interface{}, data interface{}) bool {
	key := reflect.ValueOf(keyI)
	switch reflect.TypeOf(data).Kind() {
	case reflect.Slice:
		s := reflect.ValueOf(data)
		for i := 0; i < s.Len(); i++ {
			if s.Index(i).Interface() == key.Interface() {
				return true
			}
		}
	case reflect.Map:
		mapData := reflect.ValueOf(data)
		for _, idx := range mapData.MapKeys() {
			//log.Info("compare ", idx.Interface(), key.Interface(), idx.Interface() == key.Interface())
			//log.Info("type of ", reflect.TypeOf(idx.Interface()), reflect.TypeOf(key.Interface()))
			if idx.Interface() == key.Interface() {
				return true
			}

		}
	}
	return false
}

func BodyToHex(message []byte) string {
	mac := hmac.New(sha1.New, []byte(PYRUS_KEY))
	mac.Write(message)
	return hex.EncodeToString(mac.Sum(nil))
}

func RequestIDFromContext(ctx context.Context) string {
	return ctx.Value(requestIDKey).(string)
}

func Round(x float64) float64 {
	t := math.Trunc(x)
	if math.Abs(x-t) >= 0.5 {
		return t + math.Copysign(1, x)
	}
	return t
}

func ReadSnilsFromXlsFile(path string) ([]string, error) {
	var snilsList []string
	xlFile, err := xls.Open(path, "utf-8")
	if err != nil {
		tmpMsg := fmt.Sprintf("Couldn't read file %v", path)
		log.Warning(tmpMsg)
		log.Error(err)
		return snilsList, err
	}

	sheet := xlFile.GetSheet(0)
	var tmpSnils string
	for rowNum := uint16(4); rowNum <= sheet.MaxRow; rowNum++ {
		tmpSnils = sheet.Row(int(rowNum)).Col(5)
		//fmt.Println(tmpSnils)
		//todo: append only pattern(000-000-000 0) fitted fields
		snilsList = append(snilsList, tmpSnils)

	}
	return snilsList, nil
}
